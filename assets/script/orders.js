const app = new Vue({
    el: '#app',
    data: {
      height: '83px',
      opened: false
    },
    computed: {
        computedHeight: function () {
          return this.height;
        }
      },
    methods: {
      viewAlert: function() {
        alert("Лох, но работает!");
      },

      viewOrder: function() {
          if(!this.opened)
            this.height = '400px';
          else
            this.height = '83px';
          this.opened = ! this.opened;
      },
    }
  });