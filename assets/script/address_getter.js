$(function () {

    let address = $('input[name=address]');
    let addressesList = $(".addressesList");
    let url = JSON.parse(document.getElementById('get_address_url').textContent);

    address.bind("input", function () {
        if (this.value.length > 2) {
            let old_value = this.value;
            setTimeout(function (updatedAddress) {
                let new_value = updatedAddress.value;
                if (new_value === old_value) {
                    $.ajax({
                        type: 'get',
                        url: url['url'],
                        data: {'search_value': new_value},
                        success: [function (response) {
                            let data = JSON.parse(response);
                            let text = data['response']['GeoObjectCollection']['featureMember'][0]
                                ['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']
                                ['formatted'];
                            addressesList.html("<li>" + text + "</li>").fadeIn();
                        }],
                        error: function () {
                            alert("Connection problem");
                        }
                    })
                }
            }, 555, this)
        }
    });

    addressesList.hover(function () {
        address.blur();
    });

    addressesList.on("click", "li", function () {
        let chosenAddress = $(this).text();
        address.val(chosenAddress);
        addressesList.fadeOut();
    });

});
