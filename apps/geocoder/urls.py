from django.urls import path
from .views import *

urlpatterns = [
    path('', get_address_by_address, name='get_address_by_address_url'),
]
