import requests
from django.shortcuts import render
from django.http import HttpResponse


def get_address_by_address(request):
    data = request.GET
    payload = {'geocode': data['search_value'],
               'apikey': '747a78f8-0462-4d88-a507-a0de3292429e',
               'format': 'json',
               'results': 1,
               'lang': 'en_RU',
               }
    response = requests.get('https://geocode-maps.yandex.ru/1.x/', params=payload)
    print(response.text)
    return HttpResponse(response.text)


def get_address_by_address_direct(search_value):
    payload = {'geocode': search_value,
               'apikey': '747a78f8-0462-4d88-a507-a0de3292429e',
               'format': 'json',
               'results': 1,
               'lang': 'en_RU',
               }
    response = requests.get('https://geocode-maps.yandex.ru/1.x/', params=payload)
    return response.text


# Create your views here.
