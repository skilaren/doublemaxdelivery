from django.db import models


class Place(models.Model):
    city = models.CharField(max_length=50)
    street = models.CharField(max_length=50)
    building = models.CharField(max_length=15)

    def __str__(self):
        return self.street + ", " + self.building

    def display_full(self):
        return self.city + ", " + self.street + ", " + self.building
