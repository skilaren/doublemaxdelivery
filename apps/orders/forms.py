from django import forms
from django.core.exceptions import ValidationError
from libs.Filters.filters import *


class OrderCreateForm(forms.Form):
    store_name = forms.CharField()
    store_address = forms.CharField()
    destination_address = forms.CharField()

    def clean_destination_address(self):
        address = self.cleaned_data['destination_address']

        cleaned_address = address_fullness_filter(address)

        if len(cleaned_address) < 3:
            raise ValidationError("Provide address with precision of building")

        info_json = json.dumps(cleaned_address)
        return info_json

    def clean_store_address(self):
        address = self.cleaned_data['store_address']

        cleaned_address = address_fullness_filter(address)

        if len(cleaned_address) < 3:
            raise ValidationError("Provide address with precision of building")

        info_json = json.dumps(cleaned_address)
        return info_json
