from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.shortcuts import redirect
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from libs.ObjectCreating.utils import ObjectCreateMixin
from .forms import *
from .models import *
import json


class Orders(View):

    @method_decorator(login_required)
    def get(self, request):
        orders = Order.objects.filter(courier=None).exclude(customer_id=request.user.userID)
        orders_with_products = []
        for order in orders:
            products = json.loads(order.product)
            orders_with_products.append((order, products))
        context = {
            'orders': orders_with_products,
            'user_orders': False,
        }
        return render(request, template_name='orders/view_orders.html', context=context)

    @method_decorator(login_required)
    def post(self, request):
        order_id = request.POST['order_id']
        order = Order.objects.get(orderID__exact=order_id)
        order.set_courier(request.user.userID)
        order.status = Order.TAKEN
        order.save()
        return self.get(request)


def user_orders(request):
    orders = Order.objects.filter(customer=request.user.id)
    context = {
        'orders': orders,
        'user_orders': True,
    }
    return render(request, template_name='orders/view_orders.html', context=context)


class OrderCreate(ObjectCreateMixin, View):
    form_model = OrderCreateForm
    template = 'orders/order_create.html'

    @method_decorator(login_required)
    def get(self, request):
        if request.user.is_authenticated:
            form = self.form_model()
            products = []
            products_encoded = request.session.get('products')
            if products_encoded:
                products = json.loads(products_encoded)
            context = {
                'form': form,
                'products': products,
            }
            return render(request, self.template, context=context)
        else:
            return render(request, 'profiles/user_login.html')

    @method_decorator(login_required)
    def post(self, request):
        bound_form = self.form_model(request.POST)
        if bound_form.is_valid():
            order_info = bound_form.cleaned_data
            if not request.session.get('products'):
                error = 'Enter some products'
                context = {
                    'form': bound_form,
                    'error': error
                }
                return render(request, self.template, context=context)
            if not order_info['store_name'] or not order_info['store_address']:
                error = 'Enter store info'
                context = {
                    'form': bound_form,
                    'error': error
                }
                return render(request, self.template, context=context)

            store_addr = json.loads(order_info['store_address'])

            store_place = Place(city=store_addr['locality'],
                                street=store_addr['street'],
                                building=store_addr['house']
                                )
            store_place.save()
            order = Order(destination_address=request.user.address,
                          store=order_info['store_name'],
                          store_address=store_place,
                          product=request.session.get('products'),
                          status=Order.PLACED,
                          customer_id=request.user.userID
                          )
            order.save()
            request.session['products'] = None
            return redirect('orders_view_url')
        return render(request, self.template, context={'form': bound_form})


@csrf_exempt
def add_product_in_cart(request):
    product_info = json.loads(request.body)
    products = request.session.get('products')
    if not products:
        products = '[]'
    products_json = json.loads(products)
    product = product_info['product']
    quantity = product_info['quantity']
    products_json.append({'product': product, 'quantity': quantity})
    request.session['products'] = json.dumps(products_json)

    return HttpResponse("Done")


@csrf_exempt
def remove_product_from_cart(request):
    product_info = json.loads(request.body)
    products = request.session.get('products')
    if not products:
        products = '[]'
    products_json = json.loads(products)
    product = product_info['product']
    quantity = product_info['quantity']
    products_json.remove({'product': product, 'quantity': quantity})
    request.session['products'] = json.dumps(products_json)

    return HttpResponse("Done")
