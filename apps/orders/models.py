from django.db import models
from apps.profiles.models import User
from apps.geocoder.models import Place


class Order(models.Model):
    PLACED = 'p'
    TAKEN = 't'
    DELIVERING = 'd'
    COMPLETED = 'c'

    orderID = models.AutoField(primary_key=True)
    date = models.DateTimeField(auto_now_add=True)
    store = models.CharField(max_length=150, default=None)
    store_address = models.ForeignKey(Place, related_name="store_address", default=None,
                                      blank=False, on_delete=models.CASCADE)
    destination_address = models.ForeignKey(Place, related_name="destination_address", default=None,
                                            blank=False, on_delete=models.CASCADE)
    product = models.CharField(max_length=2000)
    customer = models.ForeignKey(User, on_delete=models.CASCADE, related_name="customer", default=None)
    courier = models.ForeignKey(User, on_delete=models.CASCADE, related_name="courier", default=None, null=True)
    status_choices = (
        (PLACED, 'Placed'),
        (TAKEN, 'Taken'),
        (DELIVERING, 'Delivering'),
        (COMPLETED, 'Completed')
    )
    status = models.CharField(max_length=1, choices=status_choices, default='p')

    def set_courier(self, courier_id):
        if courier_id:
            self.courier_id = courier_id
        else:
            raise ValueError("Courier are already assigned to this order")

    def __str__(self):
        return str(self.orderID) + ": " + str(self.destination_address)
