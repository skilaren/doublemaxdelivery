"""orders App URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from .views import *

urlpatterns = [
    path('', Orders.as_view(), name='orders_view_url'),
    path('create/', OrderCreate.as_view(), name='order_create_url'),
    path('my/', user_orders, name='user_orders_url'),
    path('cart/add/', add_product_in_cart, name='order_item_add'),
    path('cart/remove/', remove_product_from_cart, name='order_item_remove'),
]
