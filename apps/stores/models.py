from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from apps.profiles.models import User
from apps.geocoder.models import Place
import time


def images_path(instance, filename):
    return get_current_timestamp() + '{0}'.format(filename)


def get_current_timestamp():
    time_st = time.gmtime(time.time())
    return time.strftime("%d%H%M%S", time_st)


class Store(models.Model):
    name = models.CharField(max_length=40)
    slug = models.SlugField(max_length=40, unique=True)
    store_thumbnail = ProcessedImageField(upload_to=images_path, blank=True,
                                          processors=[ResizeToFill(250, 250)],
                                          format='JPEG',
                                          options={'quality': 60})
    main_place = models.ForeignKey(Place, on_delete=models.PROTECT,
                                   related_name='main_place', blank=False, default=None)
    places = models.ManyToManyField(Place, blank=True)
    admin = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, default=None)

    def __str__(self):
        return self.name


class Email(models.Model):
    email = models.EmailField()
    stores = models.ManyToManyField(Store)


class Products(models.Model):
    name = models.CharField(max_length=40)
    products_thumbnail = ProcessedImageField(upload_to=images_path, blank=True,
                                             processors=[ResizeToFill(250, 250)],
                                             format='JPEG',
                                             options={'quality': 60})
    store_id = models.ForeignKey(Store, on_delete=models.CASCADE, blank=False, default=None)
    quantity = models.IntegerField()
    row = models.IntegerField()
    price = models.IntegerField()


class Phone(models.Model):
    phone = models.CharField(max_length=40)
    stores = models.ManyToManyField(Store)
