from django.urls import path
from django.conf.urls import url
from .views import *
from .models import *


urlpatterns = [
    path('', Test.as_view(), name='stores_view_url'),
    path('create/', StoreCreate.as_view(), name='store_create_url'),
    path('manage/', StoreManage.as_view(), name='store_manage_url'),

    url(r'\w*', Restaraunts.as_view(), name='store_view_individual'),

]
