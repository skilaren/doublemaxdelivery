from django import forms
from django.utils.text import slugify
from django.core.exceptions import ValidationError
from libs.Filters.filters import phone_number_filter
from .models import Store
from libs.Filters.filters import address_fullness_filter
import json


class StoreCreatingForm(forms.Form):
    widgets = {
        'name': forms.TextInput(attrs={'placeholder': 'store name'}),
        'slug': forms.TextInput(attrs={'placeholder': 'store short url'}),
        'store_thumbnail': forms.ClearableFileInput(attrs={'placeholder': 'store name'}),
        'email': forms.EmailInput(attrs={'placeholder': 'main email'}),
        'phone': forms.TextInput(attrs={'placeholder': 'main phone'}),
        'address': forms.TextInput(attrs={'placeholder': 'address'}),
    }

    name = forms.CharField(widget=widgets['name'], max_length=40)
    slug = forms.CharField(widget=widgets['slug'], max_length=40)
    email = forms.EmailField(widget=widgets['email'])
    phone = forms.CharField(widget=widgets['phone'], max_length=40)
    address = forms.CharField(widget=widgets['address'])
    store_thumbnail = forms.ImageField(widget=widgets['store_thumbnail'])

    def __init__(self, *args, **kwargs):

        super(StoreCreatingForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'dka-input input_placement'

    def clean_slug(self):
        new_slug = self.cleaned_data['slug']
        if new_slug != slugify(new_slug, allow_unicode=True):
            raise ValidationError('Slug is not correct, it may contain only letter and "-_" symbols')
        if Store.objects.filter(slug__iexact=new_slug):
            raise ValidationError('This slug exists, try to figure out the unique one')
        return new_slug

    def clean_phone(self):
        number = self.cleaned_data['phone']
        if phone_number_filter(number):
            return number
        else:
            raise ValidationError("Entered incorrect phone number")

    def clean_address(self):
        address = self.cleaned_data['address']

        cleaned_address = address_fullness_filter(address)

        if len(cleaned_address) < 3:
            raise ValidationError("Provide address with precision of building")

        info_json = json.dumps(cleaned_address)
        return info_json
