from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views import View
from .forms import *
from .models import *
import json


class Test(View):
    def get(self, request):
        stores = Store.objects.all()
        return render(request, template_name='stores/stores.html', context={'stores': stores})


class Restaraunts(View):
    template = 'stores/restaurant.html'

    def get(self, request):
        store_slug = str(request).split('/')[-2]
        store = Store.objects.filter(slug=store_slug)
        if not store:
            return redirect('stores_view_url')
        store = store[0]
        products = Products.objects.filter(store_id=store.id)
        context = {
            'store': store,
            'products': products
        }

        return render(request, template_name=self.template, context=context)

    def post(self, request):
        pass


class StoreCreate(View):
    general_form_model = StoreCreatingForm
    template = 'stores/store_create.html'

    @method_decorator(login_required)
    def get(self, request):
        general_form = self.general_form_model
        context = {
            'general_form': general_form,
        }
        return render(request, template_name=self.template, context=context)

    @method_decorator(login_required)
    def post(self, request):
        bound_form = self.general_form_model(request.POST, request.FILES)
        if bound_form.is_valid():
            data = bound_form.cleaned_data
            address = json.loads(data['address'])
            email = Email(email=data['email'])
            phone = Phone(phone=data['phone'])
            place = Place(city=address['locality'],
                          street=address['street'],
                          building=address['house'])
            email.save()
            phone.save()
            place.save()
            store = Store(name=data['name'],
                          slug=data['slug'],
                          store_thumbnail=data['store_thumbnail'],
                          main_place=place,
                          admin=request.user)
            store.save()

            store.email_set.add(email)
            store.phone_set.add(phone)

            email.save()
            phone.save()
            store.save()
            place.save()
            return redirect('main_page_url')
        else:
            context = {
                'general_form': bound_form,
            }
            return render(request, template_name=self.template, context=context)


class StoreManage(View):

    def get(self, request):
        return render(request, template_name='stores/stores_manage.html')

    def post(self, request):
        pass
