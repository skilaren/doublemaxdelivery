from django.shortcuts import render


def main_page(request):
    return render(request, template_name='food_delivery/main_page.html')
