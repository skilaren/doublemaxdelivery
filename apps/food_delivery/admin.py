from django.contrib import admin
from apps.profiles.models import *
from apps.orders.models import *
from apps.stores.models import *
from apps.geocoder.models import *


class CustomUserAdmin(admin.ModelAdmin):
    username = 'email'


admin.site.register(Order, CustomUserAdmin)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Store, CustomUserAdmin)
admin.site.register(Place, CustomUserAdmin)
admin.site.register(Products, CustomUserAdmin)
