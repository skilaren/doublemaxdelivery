from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from .forms import *
from apps.orders.models import Order
import json


class UserProfile(View):

    def get(self, request):
        orders_made = Order.objects.filter(customer_id__exact=request.user.userID)
        orders_delivered = Order.objects.filter(courier_id__exact=request.user.userID)
        context = {
            'orders_made': orders_made,
            'orders_delivered': orders_delivered,
            'orders_made_amount': len(orders_made),
            'orders_delivered_amount': len(orders_delivered),
            'user': request.user,
        }
        return render(request, template_name='profiles/user_profile.html', context=context)


def user_logout(request):
    logout(request)
    return redirect('main_page_url')


class UserLogin(View):

    def get(self, request):
        form = UserLoginForm()
        context = {
            'form': form,
        }
        return render(request, template_name='profiles/user_login.html', context=context)

    def post(self, request):
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            return redirect('user_profile_url')
        else:
            context = {
                'form': UserLoginForm(request.POST)
            }
            return render(request, template_name='profiles/user_login.html', context=context)


class UserCreate(View):
    form_model = UserForm
    template = 'profiles/user_create.html'
    success_redirect_url = 'user_login_url'

    def get(self, request):
        form = self.form_model()
        get_address_url = 'get_address_by_address_url'
        context = {
            'form': form,
            'get_address_url': get_address_url,
        }
        return render(request, self.template, context=context)

    def post(self, request):
        bound_form = self.form_model(request.POST)
        if bound_form.is_valid():
            address_json = bound_form.cleaned_data['address']
            address_dict = json.loads(address_json)

            place = Place(city=address_dict['locality'], street=address_dict['street'], building=address_dict['house'])
            place.save()
            new_user = bound_form.save(commit=False)
            new_user.address = place
            new_user.save()
            return redirect(self.success_redirect_url)
        return render(request, self.template, context={'form': bound_form})


class UserEdit(View):
    form_model = UserEditForm
    template = 'profiles/user_edit.html'
    success_redirect_url = 'user_profile_url'

    def get(self, request):
        data = {
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
            'address': request.user.address.display_full
        }
        form = self.form_model(initial=data)
        context = {
            'form': form
        }
        return render(request, template_name=self.template, context=context)

    def post(self, request):
        data = {
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
            'address': request.user.address.display_full
        }
        bound_form = self.form_model(request.POST, initial=data)
        if bound_form.is_valid():
            user = request.user
            place = user.address

            address_json = bound_form.cleaned_data['address']
            address = json.loads(address_json)

            place.city = address['locality']
            place.street = address['street']
            place.building = address['house']
            place.save()

            user.first_name = bound_form.cleaned_data['first_name']
            user.last_name = bound_form.cleaned_data['last_name']
            user.email = bound_form.cleaned_data['email']
            user.save()

            return redirect(self.success_redirect_url)
        return render(request, self.template, context={'form': bound_form})
