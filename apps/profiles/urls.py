from django.urls import path
from .views import *

urlpatterns = [
    path('sign_up/', UserCreate.as_view(), name='user_create_url'),
    path('login/', UserLogin.as_view(), name='user_login_url'),
    path('logout/', user_logout, name='user_logout_url'),
    path('edit/', UserEdit.as_view(), name='user_edit_url'),
    path('', UserProfile.as_view(), name='user_profile_url')
]
