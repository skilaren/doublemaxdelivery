from django.test import TestCase
from ..forms import UserForm


class TestUserForm(TestCase):

    def test_user_form_valid_data(self):
        valid_data = {
            "email": "maksven@mail.com",
            "password1": "secretpass",
            "password2": "secretpass",
            "first_name": "Maxim",
            "last_name": "Burov",
            "address": "aqwwqdqwqwwdqqd",
            "phone": "8978974949",
        }

        form = UserForm(valid_data)
        form.is_valid()
        self.assertFalse(form.errors)

    def test_user_form_invalid_data(self):
        invalid_data = {
            "email": "maksve#^*$n@mail.com",
            "password1": "secretpass",
            "password2": "secretpass",
            "first_name": "Maxim",
            "last_name": "Burov",
            "address": "aqwwqdqwqwwdqqd",
            "phone": "8978974qw949",
        }

        form = UserForm(invalid_data)
        form.is_valid()
        self.assertTrue(form.errors)
