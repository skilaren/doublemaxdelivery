from django import forms
from .models import *
from django.core.exceptions import ValidationError
from django.contrib.auth.password_validation import validate_password
from libs.Filters.filters import *


class UserForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(
        attrs={'placeholder': 'password'}))
    password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput(
        attrs={'placeholder': 'repeat password'}))
    address = forms.CharField(label="Address", widget=forms.TextInput(
        attrs={'placeholder': 'address'}))

    class Meta:
        model = User
        fields = ['email', 'password1', 'password2', 'first_name', 'last_name', 'phone']

        widgets = {
            'first_name': forms.TextInput(attrs={'placeholder': 'first name'}),
            'last_name': forms.TextInput(attrs={'placeholder': 'last name'}),
            'email': forms.TextInput(attrs={'placeholder': 'e-mail'}),
            'phone': forms.TextInput(attrs={'placeholder': 'phone'}),
        }

    def __init__(self, *args, **kwargs):

        super(UserForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'input input_placement'

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")

        validate_password(password=password2)

        return password2

    def clean_email(self):
        new_email = self.cleaned_data['email']

        if User.objects.filter(email__exact=new_email).count():
            raise ValidationError('This e-mail is already used')

        return new_email

    def clean_phone(self):
        number = self.cleaned_data['phone']
        if phone_number_filter(number):
            return number
        else:
            raise ValidationError("Entered incorrect phone number")

    def clean_address(self):
        address = self.cleaned_data['address']

        cleaned_address = address_fullness_filter(address)

        if len(cleaned_address) < 3:
            raise ValidationError("Provide address with precision of building")

        info_json = json.dumps(cleaned_address)
        return info_json

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password2'])
        if commit:
            user.save()
        return user


class UserLoginForm(forms.Form):
    email = forms.EmailField(label='E-mail', widget=forms.EmailInput(attrs={'class': 'dka-input',
                                                                            'placeholder': 'email'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'dka-input',
                                                                                   'placeholder': 'password'}))


class UserEditForm(forms.Form):
    widgets = {
        'first_name': forms.TextInput(attrs={'style': "height: 30px; width: 90%;", 'class': "fullnameText"}),
        'last_name': forms.TextInput(attrs={'style': "height: 30px; width: 90%;", 'class': "fullnameText"}),
        'email': forms.EmailInput(attrs={'style': "height: 30px; width: 90%;", 'class': "emailText"}),
        'address': forms.TextInput(attrs={'style': "width: 90%; position: relative; left: 10px;"}),
    }

    first_name = forms.CharField(widget=widgets['first_name'], max_length=40)
    last_name = forms.CharField(widget=widgets['last_name'], max_length=40)
    email = forms.EmailField(widget=widgets['email'])
    address = forms.CharField(widget=widgets['address'])

    def clean_address(self):
        address = self.cleaned_data['address']

        cleaned_address = address_fullness_filter(address)

        if len(cleaned_address) < 3:
            raise ValidationError("Provide address with precision of building")

        info_json = json.dumps(cleaned_address)
        return info_json

    def clean_email(self):
        new_email = self.cleaned_data['email']
        print(self.changed_data)

        if 'email' in self.changed_data and User.objects.filter(email=new_email).count():
            raise ValidationError('This e-mail is already used')

        return new_email
