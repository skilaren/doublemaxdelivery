Web-application for food delivery in Innopolis city

Mady by BS17-7-5 students:

Timur Anufriyev, 
Yaroslav Yudinskih, 
Katya Uzbekova, 
Maxim Popov, 
Maxim Burov
<br>
<br>
<br>


***Easy opening project for testing, working: ***
Call by telegram @katya_uzbekova or @broccoIi_nastya with question to run server
Open page: http://13.48.47.52:8080/
Start your work with our implementation of DeliveryClub


**Installation instruction for Linux(Ubuntu):**

Repository has requirements.txt with which you can install all necessary 
dependencies for running the project

For installing the mysqlclient you firtsly need to install:
<br>
sudo apt-get install mariadb-server mariadb-client
sudo apt-get install libmariadbclient-dev

Then connect to the mysql shell with command:
<br>
sudo mysql -u root -p

For password request you can just pass the blank password

Then you have to create database:
<br>
CREATE DATABASE dcdb CHARACTER SET utf8 COLLATE utf8_general_ci;

And now we will create with persmissions to get access to db:
<br>
GRANT ALL ON dcdb.* TO 'test_user'@'localhost' IDENTIFIED BY 'testpassword' WITH GRANT OPTION;
<br>
FLUSH PRIVILEGES;