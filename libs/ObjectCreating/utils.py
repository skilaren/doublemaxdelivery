from django.shortcuts import render
from django.shortcuts import redirect


class ObjectCreateMixin:

    form_model = None
    template = None
    success_redirect_url = None

    def get(self, request):
        form = self.form_model()
        return render(request, self.template, context={'form': form})

    def post(self, request):
        bound_form = self.form_model(request.POST)
        if bound_form.is_valid():
            bound_form.save()
            return redirect(self.success_redirect_url)
        return render(request, self.template, context={'form': bound_form})
