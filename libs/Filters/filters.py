from apps.geocoder.views import get_address_by_address_direct
import re
import json


def phone_number_filter(number):
    if re.match(r'^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$', number):
        return True
    else:
        return False


def address_fullness_filter(address):
    cleaned_address = {}

    response = get_address_by_address_direct(address)
    address_json = json.loads(response)
    try:
        address_components = address_json['response']['GeoObjectCollection']['featureMember'][0]['GeoObject'] \
            ['metaDataProperty']['GeocoderMetaData']['Address']['Components']
    except IndexError:
        return cleaned_address

    for component in address_components:
        kind = component['kind']
        if kind == 'locality' or kind == 'street' or kind == 'house':
            cleaned_address.update({kind: component['name']})

    return cleaned_address
