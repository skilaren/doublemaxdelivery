from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time


#_______________TEST ACCEPTANCE CRITERIA__________________
#Given: Developer with open laptop and with Internet
#When: Click on just link
#Then: See working project without any other tolls except Internet and Browser
def test_server():
    driver = webdriver.Chrome(executable_path="chromedriver.exe")
    driver.get("http://ec2-13-48-47-52.eu-north-1.compute.amazonaws.com:8080")
    time.sleep(5)
    driver.save_screenshot("photos/main_page.png")
    driver.close()
def sign_up():
    driver = webdriver.Chrome(executable_path="chromedriver.exe") #init
    driver.get("http://ec2-13-48-47-52.eu-north-1.compute.amazonaws.com:8080")
    driver.maximize_window()

    #time.sleep(5)
    sign_up = driver.find_element_by_xpath("/html/body/div[1]/a[2]/p")
    sign_up.click()
    driver.save_screenshot("photos/sign_up.png")
    driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
    time.sleep(5)

    mail = driver.find_element_by_xpath("//*[@id='id_email']")
    pas = driver.find_element_by_xpath("//*[@id='id_password1']")
    pas2 = driver.find_element_by_xpath("//*[@id='id_password2']")
    name = driver.find_element_by_xpath("//*[@id='id_first_name']")
    surname = driver.find_element_by_xpath("//*[@id='id_last_name']")
    phone = driver.find_element_by_xpath("//*[@id='id_phone']")
    addr = driver.find_element_by_xpath("//*[@id='id_address']")
    #variants:

    #1 ________Invalid MAIL__________
    #mail.send_keys("kat")
    #pas.send_keys("1")
    #pas2.send_keys("1")
    #name.send_keys("Katya")
    #surname.send_keys("Uzbekova")
    #phone.send_keys("89177972480")
    #addr.send_keys("Universitetskaya")
    #2 ________Already Exist Mail_______
    #mail.send_keys("katay@yandex.ru")
    #pas.send_keys("1")
    #pas2.send_keys("1")
    #name.send_keys("Katya")
    #surname.send_keys("Uzbekova")
    #phone.send_keys("89177972480")
    #addr.send_keys("Universitetskaya")
    #3________New Valid Mail___________
    mail.send_keys("noName@y.ru")
    pas.send_keys("1")
    pas2.send_keys("1")
    name.send_keys("Katya")
    surname.send_keys("Uzbekova")
    phone.send_keys("89177972480")
    addr.send_keys("Universitetskaya")
    register_button = driver.find_element_by_xpath("//*[@id='signUpSubframe']/form/button").click()
   # print(driver.title)
    assert driver.title == "Delivery Club - Login"

#_____________TEST ACCEPTANCE CRITERIA___________________________
#Given: User has registered in the system and stays on profile page
#When: User clicks Courier/Customer
#Then: Order Block switches between orders delivered and orders posted

def dist_order():
    driver = webdriver.Chrome(executable_path="chromedriver.exe")
    driver.get("http://ec2-13-48-47-52.eu-north-1.compute.amazonaws.com:8080/user/login")
    driver.maximize_window()
    mail = driver.find_element_by_xpath("//*[@id='id_email']")
    mail.send_keys("katayU@yandex.ru")
    pas = driver.find_element_by_xpath("//*[@id='id_password']").send_keys("123")
    driver.find_element_by_xpath("//*[@id='authButton']").click()
    time.sleep(3)
    checkbox = driver.find_element_by_xpath("//*[@id='switcher']").click()
    driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
    time.sleep(3)
    exit = driver.find_element_by_xpath("/html/body/button/a").click()
    time.sleep(3)
#_____________TEST ACCEPTANCE CRITERIA_________________________________
#Given: User logged in stays on main page
#When: Do nothing
#Then: User see all information and useful links to work with app at the main page

def info_main_page():
    driver = webdriver.Chrome(executable_path="chromedriver.exe")
    driver.get("http://ec2-13-48-47-52.eu-north-1.compute.amazonaws.com:8080/user/login")
    driver.maximize_window()
    mail = driver.find_element_by_xpath("//*[@id='id_email']")
    mail.send_keys("katayU@yandex.ru")
    pas = driver.find_element_by_xpath("//*[@id='id_password']").send_keys("123")
    driver.find_element_by_xpath("//*[@id='authButton']").click()
    time.sleep(3)
    driver.find_element_by_xpath("/html/body/div[1]/a[1]/p").click()
    driver.maximize_window()

    driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
    time.sleep(5)
    driver.save_screenshot("photos/main_page_info.png")


#______________TEST ACCEPTANCE CRITERIA________________________________
#Given: User logged at the website and stays on main page
#When: User clicks on "Menu"
#Then: See list of products from which user can choose
